package ip_distribution;

import java.util.*;
import java.awt.*;
import javax.swing.*;

/*
 * IP címzés - IP distribution
 * @Author Sipos F. Bence
 */

public class ip_dist extends JFrame {
    private static ArrayList<Integer> hosts = new ArrayList<>();
    private static JTextField tByte1;
    private static JTextField tByte2;
    private static JTextField tByte3;
    private static JTextField tByte4;
    private static JTextField tMask;

    private ip_dist() {
        setTitle("IP címzés");
        setSize(300,200);
        setLayout(new BorderLayout());
        setLocationRelativeTo(null);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        // Container
        JPanel pContainer = new JPanel();
        pContainer.setLayout(new GridLayout(0,1));
        add(pContainer,BorderLayout.CENTER);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Title
        JPanel pTitle = new JPanel();
        pTitle.setLayout(new FlowLayout());
        JLabel lTitle = new JLabel("Hosztok:", JLabel.CENTER);
        lTitle.setFont(new Font(lTitle.getFont().getName(),lTitle.getFont().getStyle(),12));
        pTitle.add(lTitle);
        pContainer.add(pTitle);

        // IP address panel
        JPanel pIP = new JPanel();
        pIP.setLayout(new FlowLayout());
        tByte1 = new JTextField(3);
        pIP.add(tByte1);
        JLabel lByte1 = new JLabel(".");
        pIP.add(lByte1);
        tByte2 = new JTextField(3);
        pIP.add(tByte2);
        JLabel lByte2 = new JLabel(".");
        pIP.add(lByte2);
        tByte3 = new JTextField(3);
        pIP.add(tByte3);
        JLabel lByte3 = new JLabel(".");
        pIP.add(lByte3);
        tByte4 = new JTextField(3);
        pIP.add(tByte4);
        JLabel lMask = new JLabel("/");
        pIP.add(lMask);
        tMask = new JTextField(2);
        pIP.add(tMask);
        pContainer.add(pIP);

        // Add hosts panel
        JPanel pHosts = new JPanel();
        pHosts.setLayout(new FlowLayout());
        JTextField tHosts = new JTextField(5);
        pHosts.add(tHosts);
        JButton bAdd = new JButton("Hozzáad");
        bAdd.setToolTipText("Kért hosztok hozzáadása");
        bAdd.addActionListener(e -> {
            try {
                hosts.add(Integer.parseInt(tHosts.getText()));
                tHosts.setText("");
                lTitle.setText("Hosztok: " + hosts.toString());
            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(null, "Hibás formátum!", "Hiba", JOptionPane.ERROR_MESSAGE);
            }
        });
        pHosts.add(bAdd);
        JButton bReset = new JButton("Törlés");
        bReset.setToolTipText("Kívánt hosztok törlése");
        bReset.addActionListener(e -> {
            hosts = new ArrayList<>();
            lTitle.setText("Hosztok:");
        });
        pHosts.add(bReset);
        pContainer.add(pHosts);

        // Calculate panel
        JPanel pCalc = new JPanel();
        pCalc.setLayout(new FlowLayout());
        JButton bCalc = new JButton("Kiszámítás");
        bCalc.setToolTipText("IP címek kiszámítása");
        bCalc.addActionListener(e -> calc());
        pCalc.add(bCalc);
        pContainer.add(pCalc);

        setVisible(true);
    }

    private static void calc() {
        try {
            int submask;
            try {
                submask = Integer.valueOf(tMask.getText());
            } catch(NumberFormatException el) {
                submask = 0;
            }
            if(submask >= 0 && submask <= 32) {
                if ((Integer.valueOf(tByte1.getText()) >= 0 && Integer.valueOf(tByte1.getText()) <= 255) &&
                        (Integer.valueOf(tByte2.getText()) >= 0 && Integer.valueOf(tByte2.getText()) <= 255) &&
                        (Integer.valueOf(tByte3.getText()) >= 0 && Integer.valueOf(tByte3.getText()) <= 255) &&
                        (Integer.valueOf(tByte4.getText()) >= 0 && Integer.valueOf(tByte4.getText()) <= 255)) {
                    if (hosts.size() > 1) {
                        int[] ip = {Integer.parseInt(tByte1.getText()), Integer.parseInt(tByte2.getText()), Integer.parseInt(tByte3.getText()), Integer.parseInt(tByte4.getText())};
                        int[] temp = new int[hosts.size()];
                        for (int i = 0; i < hosts.size(); i++) {
                            temp[i] = Conv.find_prov(hosts.get(i));
                        }
                        Arrays.sort(temp);
                        int[] prov = new int[temp.length];
                        int idx = 0;
                        for (int i = temp.length - 1; i >= 0; i--) {
                            prov[idx] = temp[i];
                            idx++;
                        }
                        int[] pow = new int[prov.length];
                        for (int i = 0; i < prov.length; i++) {
                            pow[i] = Conv.find_pow(prov[i]);
                        }
                        String[] mask_bin = new String[prov.length];
                        StringBuilder ip_nr = new StringBuilder();
                        StringBuilder ip_network = new StringBuilder();
                        StringBuilder ip_host = new StringBuilder();
                        StringBuilder ip_broadcast = new StringBuilder();
                        StringBuilder ip_mask = new StringBuilder();
                        for (int i = 0; i < prov.length; i++) {
                            ip_nr.append("#").append((i + 1)).append(System.lineSeparator());
                            ip_network.append(Conv.ip_to_string(ip)).append("/").append(32 - pow[i]).append(System.lineSeparator());
                            ip = Conv.incr_ip(ip);
                            ip_host.append(Conv.ip_to_string(ip)).append("\t-\t");
                            for (int j = 2; j < prov[i] - 1; j++) {
                                ip = Conv.incr_ip(ip);
                            }
                            ip_host.append(Conv.ip_to_string(ip)).append(" (").append(prov[i]).append(")").append(System.lineSeparator());
                            ip = Conv.incr_ip(ip);
                            ip_broadcast.append(Conv.ip_to_string(ip)).append(System.lineSeparator());
                            mask_bin[i] = Conv.calc_mask(32 - pow[i]);
                            ip_mask.append(Conv.ip_to_string(Conv.mask_to_dec(mask_bin[i]))).append(System.lineSeparator());
                            ip = Conv.incr_ip(ip);
                        }
                        createList(ip_nr, ip_network, ip_host, ip_broadcast, ip_mask);
                    } else if (hosts.size() == 1) {
                        int[] ip = {Integer.parseInt(tByte1.getText()), Integer.parseInt(tByte2.getText()), Integer.parseInt(tByte3.getText()), Integer.parseInt(tByte4.getText())};
                        int prov = Conv.find_prov(hosts.get(0));
                        int pow = Conv.find_pow(prov);
                        int newmask = 32 - Conv.find_pow(prov);
                        if (newmask >= submask) {
                            StringBuilder ip_nr = new StringBuilder();
                            StringBuilder ip_network = new StringBuilder();
                            StringBuilder ip_host = new StringBuilder();
                            StringBuilder ip_broadcast = new StringBuilder();
                            StringBuilder ip_mask = new StringBuilder();
                            int n = ((int) Math.pow(2, (32 - submask))) / ((int) Math.pow(2, (32 - newmask)));
                            for (int i = 0; i < n; i++) {
                                ip_nr.append("#").append((i + 1)).append(System.lineSeparator());
                                ip_network.append(Conv.ip_to_string(ip)).append("/").append(32 - pow).append(System.lineSeparator());
                                ip = Conv.incr_ip(ip);
                                ip_host.append(Conv.ip_to_string(ip)).append("\t-\t");
                                for (int j = 2; j < prov - 1; j++) {
                                    ip = Conv.incr_ip(ip);
                                }
                                ip_host.append(Conv.ip_to_string(ip)).append(" (").append(prov).append(")").append(System.lineSeparator());
                                ip = Conv.incr_ip(ip);
                                ip_broadcast.append(Conv.ip_to_string(ip)).append(System.lineSeparator());
                                ip_mask.append(Conv.ip_to_string(Conv.mask_to_dec(Conv.calc_mask(newmask)))).append(System.lineSeparator());
                                ip = Conv.incr_ip(ip);
                            }
                            createList(ip_nr, ip_network, ip_host, ip_broadcast, ip_mask);
                        } else {
                            JOptionPane.showMessageDialog(null, "Nem fér bele a hálózatba!", "Hiba", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Nincs host szám megadva!", "Hiba", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Hibás IP formátum!", "Hiba", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (NumberFormatException el) {
            JOptionPane.showMessageDialog(null, "Hibás IP formátum!", "Hiba", JOptionPane.ERROR_MESSAGE);
        }
    }

    private static void createList(StringBuilder ip_nr, StringBuilder ip_network, StringBuilder ip_host, StringBuilder ip_broadcast, StringBuilder ip_mask) {
        JFrame fList = new JFrame();
        fList.setLayout(new FlowLayout());
        JPanel pContainer = new JPanel();
        pContainer.setLayout(new FlowLayout());
        JScrollPane sContainer = new JScrollPane(pContainer);
        sContainer.setPreferredSize(new Dimension(900,500));

        // Number
        JPanel pNumber = new JPanel();
        pNumber.setLayout(new BorderLayout());
        JLabel lNumber = new JLabel("#", SwingConstants.CENTER);
        pNumber.add(lNumber,BorderLayout.NORTH);
        JTextArea tNumber = new JTextArea();
        tNumber.setFont(new Font(tNumber.getFont().getName(), tNumber.getFont().getStyle(), 11));
        tNumber.setText(ip_nr.toString());
        tNumber.setEditable(false);
        pNumber.add(tNumber,BorderLayout.SOUTH);
        pContainer.add(pNumber);
        pContainer.add(Box.createHorizontalStrut(20));

        // Network
        JPanel pNetwork = new JPanel();
        pNetwork.setLayout(new BorderLayout());
        JLabel lNetwork = new JLabel("Network", SwingConstants.CENTER);
        pNetwork.add(lNetwork,BorderLayout.NORTH);
        JTextArea tNetwork = new JTextArea();
        tNetwork.setFont(new Font(tNetwork.getFont().getName(), tNetwork.getFont().getStyle(), 11));
        tNetwork.setText(ip_network.toString());
        tNetwork.setEditable(false);
        pNetwork.add(tNetwork,BorderLayout.SOUTH);
        pContainer.add(pNetwork);
        pContainer.add(Box.createHorizontalStrut(20));

        // Hosts
        JPanel pHost = new JPanel();
        pHost.setLayout(new BorderLayout());
        JLabel lHost = new JLabel("Host", SwingConstants.CENTER);
        pHost.add(lHost,BorderLayout.NORTH);
        JTextArea tHost = new JTextArea();
        tHost.setFont(new Font(tHost.getFont().getName(), tHost.getFont().getStyle(), 11));
        tHost.setText(ip_host.toString());
        tHost.setEditable(false);
        pHost.add(tHost,BorderLayout.SOUTH);
        pContainer.add(pHost);
        pContainer.add(Box.createHorizontalStrut(20));

        // Broadcast
        JPanel pBroadcast = new JPanel();
        pBroadcast.setLayout(new BorderLayout());
        JLabel lBroadcast = new JLabel("Broadcast", SwingConstants.CENTER);
        pBroadcast.add(lBroadcast,BorderLayout.NORTH);
        JTextArea tBroadcast = new JTextArea();tBroadcast.setFont(new Font(tBroadcast.getFont().getName(), tBroadcast.getFont().getStyle(), 11));
        tBroadcast.setText(ip_broadcast.toString());
        tBroadcast.setEditable(false);
        pBroadcast.add(tBroadcast,BorderLayout.SOUTH);
        pContainer.add(pBroadcast);
        pContainer.add(Box.createHorizontalStrut(20));

        // Mask
        JPanel pMask = new JPanel();
        pMask.setLayout(new BorderLayout());
        JLabel lMask = new JLabel("Mask", SwingConstants.CENTER);
        pMask.add(lMask,BorderLayout.NORTH);
        JTextArea tMask = new JTextArea();
        tMask.setFont(new Font(tMask.getFont().getName(), tMask.getFont().getStyle(), 11));
        tMask.setText(ip_mask.toString());
        pMask.add(tMask,BorderLayout.SOUTH);
        pContainer.add(pMask);

        fList.add(sContainer);
        sContainer.getVerticalScrollBar().setValue(1);
        fList.setTitle("IP címek - " + hosts.toString());
        fList.setSize(950, 575);
        fList.setLocationRelativeTo(null);
        fList.setVisible(true);
    }

    public static void main(String[] args) {
        new ip_dist();
    }
}