package ip_distribution;

import com.sun.org.apache.xalan.internal.xsltc.runtime.InternalRuntimeError;

public class Conv {

    private Conv() {}

    // Converts a ip address to a binary stream in an array
    static String ip_to_bin(int[] ip_adr) {
        int[] ip = ip_adr.clone();
        String bin = "";
        for(int i = 0; i < ip.length; i++) {
            for(int j = 0; j < 8; j++) {
                bin += ip[i] % 2 == 0 ? "0" : "1";
                ip[i] = ip[i] >> 1;
            }
        }
        return bin;
    }

    // Converts a binary stream into a decimal integer
    static int bin_to_dec(String bin) {
        int dec = 0;
        for(int i = bin.length(); i > 0; i--) {
            if (bin.charAt(i - 1) == '1') {
                dec += Math.pow(2, (bin.length() - i));
            }
        }
        return dec;
    }

    // Finds the province of which the given amount of hosts fit in
    static int find_prov(int amount) {
        int province = 2;
        while(province / (amount+2) < 1) {
            province *= 2;
        }
        return province;
    }

    // Finds the power(2) of the given number
    static int find_pow(int number) {
        int result = 0;
        if (number % 2 == 0) {
            while (number / 2 > 0) {
                result++;
                number /= 2;
            }
        }
        return result;
    }

    // Converts the IP address to integers divided in an array
    static int[] convert_ip(String ip) {
        int[] bytes = new int[4];
        String temp = "";
        int idx = 0;
        for (int i = 0; i < ip.length(); i++) {
            if (Character.isDigit(ip.charAt(i))) {
                temp += ip.charAt(i);
            } else if (ip.charAt(i) == '.') {
                bytes[idx] = Integer.parseInt(temp);
                idx++;
                temp = "";
            }
        }
        bytes[idx] = Integer.parseInt(temp);
        return bytes;
    }

    // Creates a binary mask from the given subnet mask
    static String calc_mask(int submask) {
        String mask = "";
        for(int i = 0; i < 32; i++) {
            mask += i < submask ? "1" : "0";
        }
        //int[] finalMask = Conv.mask_to_dec(mask);
        //return Conv.ip_to_bin(finalMask);
        return mask;
    }

    // Converts a binary mask to integers in an array
    static int[] mask_to_dec(String mask) {
        String temp = "";
        int[] dec = new int[4];
        int idx = 0;
        for(int i = 0; i < mask.length(); i += 8) {
            for(int j = 0; j < 8; j++) {
                temp += mask.charAt(i+j);
            }
            dec[idx] += Conv.bin_to_dec(temp);
            temp = "";
            idx++;
        }
        return dec;
    }

    // Increments an IP address by 1
    static int[] incr_ip(int[] ip) {
        if(ip[3] < 255) {
            ip[3]++;
        } else if(ip[2] < 255) {
            ip[3] = 0;
            ip[2]++;
        } else if(ip[1] < 255) {
            ip[3] = 0;
            ip[2] = 0;
            ip[1]++;
        } else if(ip[0] < 255) {
            ip[3] = 0;
            ip[2] = 0;
            ip[1] = 0;
            ip[0]++;
        } else {
            //throw new InternalRuntimeError("Out of IP addresses.");
            return ip;
        }
        return ip;
    }

    static boolean find_network(String ip, String mask) {
        String temp = "";
        for(int i = 0; i < ip.length(); i++) {
            temp += ip.charAt(i) == '1' && mask.charAt(i) == '1' ? "1" : "0";
        }
        return temp.equals(ip);
    }

    static String ip_to_string(int[] ip) {
        String s = "";
        for(int i = 0; i < ip.length; i++) {
            s += ip[i];
            if(i != ip.length-1) {
                s += ".";
            }
        }
        return s;
    }

    // Increments an IP address by a given increment
    static int[] incr_ip(int[] ip, int incr) {
        int idx = 3;
        while(incr / 256 > 255) {
            idx--;
            incr /= 256;
        }
        if(ip[idx]+incr > 255) {
            if(idx-1 >= 0) {
                ip[idx-1] += (ip[idx]+incr)-255;
                while(idx <= 4) {
                    ip[idx] = 0;
                    idx++;
                }
            } else {
                throw new InternalRuntimeError("Out of IP addresses.");
            }
        } else {
            ip[idx] += incr;
        }
        return ip;
    }
}
